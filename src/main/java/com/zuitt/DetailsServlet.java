package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	public void init() throws ServletException{
		System.out.println("***************************************");
		System.out.println(" DetailsServlet has been initialized");
		System.out.println("***************************************");
	}
	public void destroy(){
		System.out.println("***************************************");
		System.out.println(" DetailsServlet has been destroyed. ");
		System.out.println("***************************************");
	}
	public void doGet(HttpServletRequest req,HttpServletResponse res)throws IOException{
	
		ServletContext srvContext = getServletContext();
		String branding = srvContext.getInitParameter("branding");
		String firstN = System.getProperty("fName"); //firstname
		HttpSession session = req.getSession();
		String lastN =session.getAttribute("lName").toString(); //lastname
		PrintWriter out= res.getWriter();
		String email = (String) srvContext.getAttribute("email");
		String contact = System.getProperty("contact");
		out.println("<h1>Welcome to PhoneBook</h1>"+"<p>Branding: "+branding+"</p>"+"<p>First Name: "+firstN+"</p>"+"<p>Last Name: "+lastN+"</p>"+
				"<p>Email: "+email+"</p>"+"<p>Contact:"+contact+"</p>");
	}
}
